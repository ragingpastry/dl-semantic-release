# dl-semantic-release

Builds docker container image containing the `semantic-release` tool and some plugins.


## Purpose

The Docker container built from this repository can be used to perform automated semantic releases of a project using the `semantic-release` tool.

## Usage

### Semantic Release

The `semantic-release` tool can be used to perform automated version bumps and CHANGELOG generation based on [Angular Style commit messages](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)

It is intended for the tool to be used as a part of a CI/CD pipeline.

Below in an example of a `.gitlab-ci.yml` configuration file which enables `semantic-release` on the repository. 

```yml
generate_release:
  stage: generate_release
  services:
    - docker:18.09-dind
  image: registry.gitlab.com/dreamer-labs/dl-semantic-release:latest
  script:
    - mkdir ~/.ssh && chmod 0700 ~/.ssh
    - echo "$DEPLOY_KEY" > ~/.ssh/id_rsa
    - chmod 0600 ~/.ssh/id_rsa
    - ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    - eval `ssh-agent`
    - ssh-add
    - semantic-release --repository-url git@gitlab.com:<your repo>.git
  only:
    refs:
      - master
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^chore/
    refs:
      - tags
```

It should be noted that a `DEPLOY_KEY` environment variable is required. This variable should contain a private SSH key which can authenticate to GitLab. This is used to push up new Git tags.
For information on how to use deploy keys in a Gitlab-CI pipeline please refer to the documentation located [here](https://docs.gitlab.com/ee/ssh/#deploy-keys)

We only run `generate_release` on the master branch, and skip running when we are running on a new tag, or when the commit message starts with chore. `semantic-release` is smart enough to only generate 
new releases when required, however it can still clutter up the job queue when not skipped.


## Features

### Distribution

The image is based off of the node alpine image.

### Packages

This image contains the following additional plugins for `semantic-release`

  - https://github.com/semantic-release/git
  - https://github.com/semantic-release/exce
  - https://github.com/semantic-release/release-notes-generator
  - https://github.com/semantic-release/changelog
  - https://github.com/semantic-release/gitlab

